#!/usr/bin/make -f

include /usr/share/javahelper/java-vars.mk

DEB_PKGNAME := $(shell dpkg-parsechangelog -SSource)
DEB_VERSION := $(shell dpkg-parsechangelog -SVersion)
DEB_NOEPOCH_VERSION := $(shell echo $(DEB_VERSION) | cut -d: -f2-)
DEB_UPSTREAM_VERSION := $(shell echo $(DEB_NOEPOCH_VERSION) | sed 's/-[^-]*$$//')
GIT_TAG := $(shell echo '$(DEB_UPSTREAM_VERSION)' | sed -e 's/~/_/')
UPSTREAM_GIT = https://github.com/ring-clojure/ring-mock

MDWN_DOCS = $(patsubst %.md,%.html,$(wildcard $(CURDIR)/*.md))

PRODUCED_JAR=ring-mock.jar
export CLASSPATH=/usr/share/java/clojure.jar:/usr/share/java/ring-codec.jar:/usr/share/java/cheshire.jar

%:
	dh $@ --with javahelper --with jh_maven_repo_helper

override_jh_build: $(MDWN_DOCS)
	jar cf $(PRODUCED_JAR) -C src .
	mkdir -p $(CURDIR)/doc/html && mv $(CURDIR)/*.html $(CURDIR)/doc/html

override_jh_classpath:
	jh_classpath $(PRODUCED_JAR)

override_jh_clean:
	jh_clean
	rm -f $(CURDIR)/$(PRODUCED_JAR)
	rm -rf $(CURDIR)/doc/html

%.html:%.md
	cat debian/header.html > $@
	sed -i'' -e 's#@TITLE@#$(shell head -n 1 $< | sed 's/^#*\s*//')#g' $@
	markdown $< >> $@
	cat debian/footer.html >> $@

override_dh_auto_test:
	dh_auto_test
	(cd test && find . -name "*.clj" | \
		xargs clojure -cp $(CURDIR)/$(PRODUCED_JAR):$(CLASSPATH))

gen-orig-xz:
	git tag -v $(GIT_TAG) || true
	if [ ! -f ../$(DEBPKGNAME)_$(VERSION).orig.tar.xz ] ; then \
		git archive --prefix=$(DEB_PKGNAME)-$(DEB_UPSTREAM_VERSION)/ $(GIT_TAG) | xz >../$(DEB_PKGNAME)_$(DEB_UPSTREAM_VERSION).orig.tar.xz ; \
		mkdir -p ../build-area ; \
		cp ../$(DEB_PKGNAME)_$(DEB_UPSTREAM_VERSION).orig.tar.xz ../build-area ; \
	fi

fetch-upstream-remote:
ifeq (,$(findstring https:,$(UPSTREAM_GIT)))
	$(error Using insecure proto in UPSTREAM_GIT: $(UPSTREAM_GIT))
endif
	git remote add upstream $(UPSTREAM_GIT) || true
	git remote set-url upstream $(UPSTREAM_GIT)
	git fetch upstream
